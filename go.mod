module bitbucket.org/viewDelta/products

go 1.14

require (
	bitbucket.org/viewDelta/service-core v0.0.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/go-playground/validator/v10 v10.3.0
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/labstack/echo/v4 v4.1.16
	github.com/labstack/gommon v0.3.0
	github.com/swaggo/echo-swagger v1.0.0
	github.com/swaggo/swag v1.6.7
	go.mongodb.org/mongo-driver v1.3.4
)

replace bitbucket.org/viewDelta/service-core => ../service-core
