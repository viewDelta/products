package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	_ "bitbucket.org/viewDelta/products/docs"
	"bitbucket.org/viewDelta/products/handlers"
	"bitbucket.org/viewDelta/service-core/config"
	endpoints "bitbucket.org/viewDelta/service-core/constants"
	settings "bitbucket.org/viewDelta/service-core/constants"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title Products
// @description This is service for product related management.
// @version 1.0
// @contact.name Abinash Pattajoshi
// @host localhost:8088
// @BasePath /products
// @tag.name products
// @tag.description Products module
// @schemes http

func main() {
	//Read Property Loader
	prop := config.GetConfiguration()
	defer config.CloseLogFile()
	//Echo Router
	router := echo.New()
	router.Use(
		middleware.Secure(),
		middleware.Recover(),
		middleware.RequestID(),
	)
	router.Debug = true
	router.HTTPErrorHandler = func(err error, c echo.Context) {
		// Take required information from error and context and send it to a service like New Relic
		log.Error(c.Path(), c.QueryParams(), err.Error())
		// Call the default handler to return the HTTP response
		router.DefaultHTTPErrorHandler(fmt.Errorf(settings.APPLICATION_ERROR), c)
	}
	router.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowCredentials: true,
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept,
			settings.TOKEN_HEADER, settings.X_Forwarded_Host, settings.HEADER_Email,
			settings.HEADER_FirstName, settings.HEADER_LastName,
			settings.HEADER_CATEGORY, settings.HEADER_PRODUCT_MAPPING},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}),
		middleware.GzipWithConfig(middleware.GzipConfig{
			Level: settings.GzipLevel,
		}))
	router.Logger.SetLevel(log.DEBUG)
	distroHandler := handlers.NewDistro()
	typeHandler := handlers.NewProductTypeHandler()
	productMappingHandler := handlers.NewProductMappingHandler()

	//	router.POST(endpoints.Products, productHandler.CreateProduct)

	group := router.Group(endpoints.Products)
	{
		// Distributor interaction apis'
		group.POST(endpoints.Distro, distroHandler.CreateDistro)
		group.GET(endpoints.Distro, distroHandler.FetchDistro)
		group.PUT(endpoints.Distro, distroHandler.UpdateDistro)
		group.DELETE(endpoints.Distro, distroHandler.DropDistro)

		// Product-Category interaction apis'
		group.POST(endpoints.Category, typeHandler.CreateCategory)
		group.GET(endpoints.Category, typeHandler.FetchCategory)
		group.PUT(endpoints.Category, typeHandler.UpdateCategory)
		group.DELETE(endpoints.Category, typeHandler.DropCategory)

		// Product-Mapping endpoints
		group.POST(endpoints.Type, productMappingHandler.CreateMapping)
		group.PUT(endpoints.Type, productMappingHandler.UpdateMapping)
		group.DELETE(endpoints.Type, productMappingHandler.DropMapping)
		group.GET(endpoints.Type, productMappingHandler.FetchMapping)
	}
	router.GET("/swagger/*", echoSwagger.WrapHandler)

	log.Infof("%v-%v Started on --> %v", prop.Application.Name,
		prop.Application.Version, prop.Application.Port)
	// Start server
	go func() {
		if err := router.Start(":" + prop.Application.Port); err != nil {
			log.Infof("shutting down the server %v", err)
		}
	}()
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := router.Shutdown(ctx); err != nil {
		router.Logger.Fatal(err)
	}
}
