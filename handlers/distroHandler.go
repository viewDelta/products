package handlers

import (
	"bitbucket.org/viewDelta/service-core/intercept"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	helper "bitbucket.org/viewDelta/service-core/app-helper"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/constants/enums"
	_ "bitbucket.org/viewDelta/service-core/locales"
	"bitbucket.org/viewDelta/service-core/locales/request"
	"bitbucket.org/viewDelta/service-core/locales/response"
	_ "bitbucket.org/viewDelta/service-core/locales/response"
	"bitbucket.org/viewDelta/service-core/models"
	"bitbucket.org/viewDelta/service-core/repository/distributor"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	iDistributorDAO = distributor.New()
)

type filterCondition struct {
	ID        string
	Email     string
	FirstName string
	LastName  string
}

func (condition *filterCondition) String() string {
	info, _ := json.Marshal(condition)
	return fmt.Sprintf("%+v\n", string(info))
}

type distro struct{}
// NewDistro will return the handler for distributer mgnt. api
func NewDistro() DistroHandler {
	return &distro{}
}

// CreateDistro creates new distributor in database
// @Summary Creates new distributor in database
// @Description  Creates new distributor in database
// @Tags products
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Param CreateDistributor body []request.CreateDistroDTOReq true "Creates new distributor"
// @Success 201 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} response.UpdateResponse "Either header is Missing or Invalid or request body contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/distributor [post]
func (object *distro) CreateDistro(c echo.Context) error {
	var (
		reqBody  []request.CreateDistroDTOReq
	)
	log.Info("Entering handlers.CreateDistro() function")
	if err := c.Bind(&reqBody); err != nil {
		return helper.ErrorHandler(c)
	}
	if err:= intercept.SecurityFilter(c);err!=nil{
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized,constants.UNAUTHORISED_ACCESS))
	}
	if errMsg := processCreateDistro(c,reqBody); errMsg != nil {
		return c.JSON(http.StatusBadRequest, errMsg)
	}
	log.Info("Exiting handlers.CreateDistro() function")
	return helper.SendSuccessResponse(c)
}

// UpdateDistro updates distributors details in database
// @Summary UpdateDistro updates distributors details in database
// @Description UpdateDistro updates distributors details in database
// @Tags products
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Param UpdateDistributor body request.UpdateDistroReq true "update distributor details"
// @Success 200 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} response.UpdateResponse "Either header is Missing or Invalid or request body contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/distributor [put]
func (object *distro) UpdateDistro(c echo.Context) error {
	log.Info("Entering handlers.UpdateDistro() function")
	var (
		reqBody  request.UpdateDistroReq
	)
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized,constants.UNAUTHORISED_ACCESS))
	}
	if err := c.Bind(&reqBody); err != nil {
		return helper.ErrorHandler(c)
	}
	log.Infof("Update distro request - {%v}", reqBody)
	if err:=processUpdate(c,reqBody);err!=nil{
		return err
	}
	log.Info("Exiting handlers.UpdateDistro() function")
	return helper.SendSuccessResponse(c)
}

// FetchDistro retrives distributors from database
// @Summary FetchDistro retrives distributors from database
// @Description FetchDistro retrives distributors from database
// @Tags products
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Param Email header string false "email"
// @Param FirstName header string false "firstName"
// @Param LastName header string false "lastName"
// @Success 200 {object} response.FetchDistroResponse "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/distributor [get]
func (object *distro) FetchDistro(c echo.Context) error {
	log.Info("Entering handlers.FetchDistro() function")
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized,constants.UNAUTHORISED_ACCESS))
	}
	email := strings.TrimSpace(c.Request().Header.Get(constants.HEADER_Email))
	firstName := strings.TrimSpace(c.Request().Header.Get(constants.HEADER_FirstName))
	lastName := strings.TrimSpace(c.Request().Header.Get(constants.HEADER_LastName))
	filterCondition := filterCondition{
		Email:     email,
		FirstName: firstName,
		LastName:  lastName,
	}
	log.Infof("Request Received for - %+v", filterCondition.String())
	return processFetch(c,filterCondition)
}

// DropDistro deltes distributors from database
// @Summary DropDistro deltes distributors from database
// @Description DropDistro deltes distributors from database
// @Tags products
// @Accept json
// @Produce json
// @Param Token header string true "session token"
// @Success 200 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/distributor [delete]
func (object *distro) DropDistro(c echo.Context) error {
	log.Info("Entering handlers.DropDistro() function")
	var (
		reqBody  request.DeleteOnIDReq
	)
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized,constants.UNAUTHORISED_ACCESS))
	}
	if err := c.Bind(&reqBody); err != nil {
		return helper.ErrorHandler(c)
	}
	if err := reqBody.Validate(validatorInstance); err != nil {
		return helper.SendGenericResponse(c, http.StatusBadRequest, err.Error())
	}
	if err := iDistributorDAO.DeleteByID(reqBody.ID); err != nil {
		return helper.ErrorHandler(c)
	}
	log.Info("Exiting handlers.DropDistro() function")
	return nil
}

func processUpdate(context echo.Context, reqBody request.UpdateDistroReq) error {
	log.Info("Entering handlers.processUpdate() function")
	// Fetch all distro from database
	allDistros,err:=iDistributorDAO.FindAll()
	if err!=nil{
		log.Errorf("Error while fetching all distros' - [%v]",err)
		return helper.ErrorHandler(context)
	}
	// Basic validation for update
	if err := reqBody.Validate(validatorInstance,allDistros); err != nil {
		return context.JSON(http.StatusBadRequest,
			helper.CustomResponseObject(http.StatusBadRequest,err.Error()))
	}
	// Record fetch check from DB
	distro, err := iDistributorDAO.FindByID(reqBody.ID)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			log.Errorf("Record Fetch error for ID - %v, error -%v", reqBody.ID, err)
			return helper.SendGenericResponse(context, http.StatusNotFound, constants.DATA_NOT_FOUND+" for id - "+reqBody.ID)
		}
		log.Errorf("Record Fetch Error  - %v", err)
		return helper.ErrorHandler(context)
	} else if _, err := iDistributorDAO.FindByEmail(reqBody.Email); err != mongo.ErrNoDocuments {
		return helper.SendGenericResponse(context, http.StatusBadRequest, constants.EMAIL_ALREADY_EXISTS)
	}
	// process update
	if strings.TrimSpace(reqBody.Address) != "" {
		distro.Address = reqBody.Address
	}
	if strings.TrimSpace(reqBody.Email) != "" {
		distro.Email = reqBody.Email
	}
	if nil != reqBody.ContactInfo {
		switch reqBody.ContactUpdateType {
		case enums.REPLACE:
			distro.ContactInfo = nil
		}
		for _, contact := range reqBody.ContactInfo {
			distro.ContactInfo = append(distro.ContactInfo, models.ContactInfo{
				CountryCode: contact.CountryCode,
				ContactNo:   contact.ContactNo,
			})
		}
	}
	if err := iDistributorDAO.Update(distro); err != nil {
		log.Errorf("Record update error - %v", err)
		return helper.ErrorHandler(context)
	}
	log.Info("Exiting handlers.processUpdate() function")
	return nil
}

// processFetch to retrive all distro records from database
func processFetch(context echo.Context, filter filterCondition) error {
	log.Info("Entering handlers.processFetch() function")
	var (
		resp       response.FetchDistroResponse
		distroList []models.Distributor
		err        error
	)
	processContentForFetchDistro := func(distroList []models.Distributor) error {
		checkEmptyList := func(distroList []models.Distributor) bool {
			if len(distroList) == 0 {
				log.Info("Distro List is empty based on condition")
				resp.Code = http.StatusNotFound
				resp.Msg = constants.RECORD_NOT_FOUND_ERROR
				resp.DistroDetails = []response.FetchDistro{}
				return true
			}
			resp.Code = http.StatusOK
			resp.Msg = constants.RECORD_FETCHED_SUCCESS
			return false
		}
		if !checkEmptyList(distroList) {
			if err := utils.UseObjectMapper(distroList, &resp.DistroDetails); err != nil {
				log.Errorf("Object Mapper Error - %v", err)
				return helper.ErrorHandler(context)
			}
			for _, v := range resp.DistroDetails {
				v.FormContactAppend()
			}
		}
		return nil
	}
	dbErrorHandler := func(err error) error {
		if err == mongo.ErrNoDocuments {
			log.Error("No records can be found")
			return processContentForFetchDistro(distroList)
		} else if err != nil {
			log.Errorf("Record Fetch Error - %v", err)
			return helper.ErrorHandler(context)
		}
		return nil
	}
	if (filterCondition{}) == filter {
		log.Info("Fetching all records as conditions are empty")
		distroList, err = iDistributorDAO.FindAll()
		if err = dbErrorHandler(err); err != nil {
			return err
		} else if err = processContentForFetchDistro(distroList); err != nil {
			return err
		}
	} else if uniqueDistro, ok, fetchErr := checkIfExists(filter); ok {
		// check for firstName or lastName
		if (utils.IsBlankString(filter.FirstName) && utils.IsBlankString(filter.LastName)) ||
			(!utils.IsBlankString(filter.FirstName) && filter.FirstName == uniqueDistro.FirstName && utils.IsBlankString(filter.LastName)) ||
			(!utils.IsBlankString(filter.LastName) && filter.LastName == uniqueDistro.LastName && utils.IsBlankString(filter.FirstName)) ||
			(!utils.IsBlankString(filter.FirstName) && !utils.IsBlankString(filter.LastName) && uniqueDistro.FirstName == filter.FirstName && uniqueDistro.LastName == filter.LastName) {
			distroList = append(distroList, *uniqueDistro)
		}
		if err = processContentForFetchDistro(distroList); err != nil {
			return err
		}
		if fetchErr != nil { // Incase, method has returned the error but it has email or nickName
			if err = processContentForFetchDistro(distroList); err != nil {
				return err
			}
		}
	} else {
		var similarNameDistroList []models.Distributor
		if !utils.IsBlankString(filter.FirstName) {
			similarNameDistroList, err = iDistributorDAO.FindByFirstName(filter.FirstName)
		} else if !utils.IsBlankString(filter.LastName) {
			similarNameDistroList, err = iDistributorDAO.FindByLastName(filter.LastName)
		}
		if nil == dbErrorHandler(err) {
			for _, eachDistro := range similarNameDistroList {
				if (!utils.IsBlankString(filter.FirstName) && eachDistro.FirstName == filter.FirstName) ||
					(!utils.IsBlankString(filter.LastName) && eachDistro.LastName == filter.LastName) {
					distroList = append(distroList, eachDistro)
				}
			}
			if err = processContentForFetchDistro(distroList); err != nil {
				return err
			}
		}
	}
	log.Info("Exiting handlers.processFetch() function")
	return context.JSON(http.StatusOK, &resp)
}

// processCreateDistro with all or nothing principal
func processCreateDistro(context echo.Context, reqBody []request.CreateDistroDTOReq) interface{} {
	log.Info("Entering handlers.processCreateDistro() function")
	if validationResponse := validateCreateDistroPayload(context,reqBody); validationResponse != nil {
		return validationResponse
	}
	log.Info("Processing Creation Distro Request")
	for _, v := range reqBody {
		var newDistroObject models.Distributor
		if err := utils.UseObjectMapper(v, &newDistroObject); err != nil {
			log.Errorf("JSON Marshalling Error : Data - {%v}, Error - %v", v, err)
			return helper.ErrorHandler(context)
		} else if err := iDistributorDAO.Insert(&newDistroObject); err != nil { // process record
			log.Errorf("Record Insertion error - %v", err)
			return helper.ErrorHandler(context)
		}
	}
	log.Info("Exiting handlers.processCreateDistro() function")
	return nil
}

// validateCreateDistroPayload will check if data has some validation issue
func validateCreateDistroPayload(context echo.Context,reqBody []request.CreateDistroDTOReq) interface{} {
	log.Info("Entering handlers.validateCreateDistroPayload() function")
	var errDetails []response.Details
	// Fetch all distro from database
	allDistros,err:= iDistributorDAO.FindAll()
	if err!=nil{
		log.Errorf("Error while fetching all distros' - [%v]",err)
		return helper.ErrorHandler(context)
	}
	for _, createDistro := range reqBody {
		if err := createDistro.Validate(validatorInstance,allDistros); err != nil {
			errDetails = append(errDetails, response.NewErrorDetail(http.StatusBadRequest, err.Error()))
			break
		}
		if _, ok, _ := checkIfExists(filterCondition{
			Email: strings.TrimSpace(createDistro.Email),
		}); ok {
			errDetails = append(errDetails, response.NewErrorDetail(http.StatusBadRequest,
				fmt.Sprintf(constants.DETAILS_ALREADY_PRESENT+" for email - %v", createDistro.Email)))
		}
	}
	if len(errDetails) != 0 {
		return response.FormUpdateResponse(http.StatusBadRequest, constants.REQUEST_PROCESSING_ERROR, errDetails)
	}
	log.Info("Exiting handlers.validateCreateDistroPayload() function")
	return nil
}

// checkIfExists will check presence of distributor based on id, nickName or email
func checkIfExists(filterCondition filterCondition) (*models.Distributor, bool, error) {
	log.Info("Entering handlers.checkIfExists() function")
	var (
		distro *models.Distributor
		err    error
	)
	validateErr := func(err error) error {
		if err == mongo.ErrNoDocuments {
			return fmt.Errorf(constants.RECORD_NOT_FOUND_ERROR)
		} else if err != nil {
			return fmt.Errorf(constants.APPLICATION_ERROR)
		}
		return nil
	}
	log.Infof("Filter Condition - %v", filterCondition.String())
	if utils.IsBlankString(filterCondition.ID) && utils.IsBlankString(filterCondition.Email) {
		log.Info("Record Can't be fetched for empty conditions")
		return nil, false, nil
	}
	if !utils.IsBlankString(filterCondition.ID) {
		distro, err = iDistributorDAO.FindByID(filterCondition.ID)
		if distro != nil && !utils.IsBlankString(filterCondition.Email) && distro.Email != filterCondition.Email {
			return nil, false, fmt.Errorf(constants.RECORD_NOT_FOUND_ERROR)
		}
	}
	if !utils.IsBlankString(filterCondition.Email) {
		distro, err = iDistributorDAO.FindByEmail(filterCondition.Email)
	}
	log.Info("Exiting handlers.checkIfExists() function")
	if errMsg := validateErr(err); errMsg != nil {
		return nil, false, errMsg
	}
	return distro, true, nil
}
