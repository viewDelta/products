package handlers

import (
	"bitbucket.org/viewDelta/service-core/locales/request"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)
// DistroHandler will contain all the distro mgnt. handler
type DistroHandler interface {
	CreateDistro(c echo.Context) error
	UpdateDistro(c echo.Context) error
	FetchDistro(c echo.Context) error
	DropDistro(c echo.Context) error
}

// ProductTypeHandler will contain all product-Type handler
type ProductCategoryHandler interface{
	CreateCategory(c echo.Context) error
	UpdateCategory(c echo.Context) error
	FetchCategory(c echo.Context) error
	DropCategory(c echo.Context) error
}

// ProductMappingHandler will contain all product-Mapping handler
type ProductMappingHandler interface{
	CreateMapping(c echo.Context) error
	UpdateMapping(c echo.Context) error
	FetchMapping(c echo.Context) error
	DropMapping(c echo.Context) error
}

var (
	validatorInstance *validator.Validate
)

func init(){
	//Make it global for caching
	validatorInstance = validator.New()
	validatorInstance.RegisterValidation("IsValidRequestType", request.IsValidRequestType)
}