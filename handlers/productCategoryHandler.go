package handlers

import (
	"encoding/hex"
	"fmt"
	"net/http"
	"strings"

	helper "bitbucket.org/viewDelta/service-core/app-helper"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/constants/enums"
	"bitbucket.org/viewDelta/service-core/intercept"
	_ "bitbucket.org/viewDelta/service-core/locales"
	"bitbucket.org/viewDelta/service-core/locales/request"
	"bitbucket.org/viewDelta/service-core/locales/response"
	_ "bitbucket.org/viewDelta/service-core/locales/response"
	"bitbucket.org/viewDelta/service-core/models"
	productcategory "bitbucket.org/viewDelta/service-core/repository/product-category"
	"bitbucket.org/viewDelta/service-core/utils"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

var (
	iProductCategoryDAO = productcategory.New()
)

type productcategoryHandler struct{}

// NewProductTypeHandler will return productType interface
func NewProductTypeHandler() ProductCategoryHandler {
	return &productcategoryHandler{}
}

// CreatePType creates new product-category in database
// @Summary CreatePType creates new product-category in database
// @Description  CreatePType creates new product-category in database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param CreateProductCategoryDTORequest body []request.CreateProductCategoryDTORequest true "Creates new productCategory"
// @Success 201 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 202 {object} response.UpdateResponse "request processed partially"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} response.UpdateResponse "Either header is Missing or Invalid or request body contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/category [post]
func (object *productcategoryHandler) CreateCategory(c echo.Context) error {
	log.Info("Entering ProductTypeHandler.CreateCategory() function")
	var (
		requestBody []request.CreateProductCategoryDTORequest
		errDetails  []response.Details
	)
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	if err := c.Bind(&requestBody); err != nil {
		log.Errorf("Request Body binding error - %v", err)
		return helper.ErrorHandler(c)
	}
	log.Infof("ProductCategry request received - %v", requestBody)
	user := intercept.GetUserFromToken(c)
	if user == nil {
		// error message if user isn't fetched
		return helper.ErrorHandler(c)
	}
	for _, eachProductCategoryDTO := range requestBody {
		if err := validatorInstance.Struct(&eachProductCategoryDTO); err != nil {
			log.Errorf("invalid payload received - %v", err)
			errDetails = append(errDetails, response.NewErrorDetail(http.StatusBadRequest, fmt.Sprintf("%v for product type - %v",
				utils.ConvertValidationErrToString(err).Error(), eachProductCategoryDTO.CategoryName)))
			break
		}
		if _, ok, err := validatePCategoryIsPresent(eachProductCategoryDTO.CategoryName); !ok {
			// Checking for server error
			if err != fmt.Errorf(constants.DATA_NOT_FOUND) && err != nil {
				return helper.ErrorHandler(c)
			}
			isValidEntry := true
			// Type validation whether it's present in DB or not
			for _, eachDistroID := range eachProductCategoryDTO.DistributorIDs {
				if err := validateDistroID(eachDistroID); err != nil {
					errDetails = append(errDetails, response.NewErrorDetail(http.StatusBadRequest,
						fmt.Sprintf("%v for product type - %v",
							err.Error(), eachProductCategoryDTO.CategoryName)))
					isValidEntry = false
				}
			}
			//Process valid records here
			if isValidEntry {
				var productTypeDB models.ProductCategory
				if err := utils.UseObjectMapper(eachProductCategoryDTO, &productTypeDB); err != nil {
					log.Errorf("Mapper Error - %v", err)
					return helper.ErrorHandler(c)
				}
				productTypeDB.CategoryName = strings.ToUpper(productTypeDB.CategoryName)
				productTypeDB.WorkFlowStatus = append(productTypeDB.WorkFlowStatus, *models.NewWorkFlowStatus(user.Email, enums.CREATED))
				log.Infof("Inserting Record - %v into - %v", productTypeDB.String(), productTypeDB.CollectionName())
				if err := iProductCategoryDAO.Insert(&productTypeDB); err != nil {
					log.Errorf("DB record insertion error - %v", err)
					return helper.ErrorHandler(c)
				}
			}
		} else {
			errDetails = append(errDetails, response.NewErrorDetail(http.StatusBadRequest,
				fmt.Sprintf("%v for product type - %v",
					constants.DETAILS_ALREADY_PRESENT, eachProductCategoryDTO.CategoryName)))
		}
	}
	if len(errDetails) != 0 {
		if len(errDetails) == len(requestBody) {
			return c.JSON(http.StatusBadRequest,
				response.FormUpdateResponse(http.StatusBadRequest, constants.INVALID_PAYLOAD, errDetails))
		}
		return c.JSON(http.StatusAccepted,
			response.FormUpdateResponse(http.StatusAccepted, constants.RECORD_PROCESSED_PARTIALLY, errDetails))
	}
	log.Info("Exiting ProductTypeHandler.CreateCategory() function")
	return c.JSON(http.StatusOK, helper.GetSuccessObject())
}

// UpdateCategory will update product-category in database
// @Summary UpdateCategory will update product-category in database
// @Description  UpdateCategory will update product-category in database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param UpdateProductCategoryDTORequest body request.UpdateProductCategoryDTORequest true "Updates existing productCategory"
// @Success 200 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 202 {object} response.UpdateResponse "request processed partially"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} response.UpdateResponse "Either header is Missing or Invalid or request body contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/category [put]
func (object *productcategoryHandler) UpdateCategory(c echo.Context) error {
	log.Info("Entering ProductTypeHandler.UpdateCategory() function")
	var requestBody request.UpdateProductCategoryDTORequest
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	if err := c.Bind(&requestBody); err != nil {
		log.Errorf("Request Body binding error - %v", err)
		return helper.ErrorHandler(c)
	}
	log.Infof("ProductCategry request received - %v", requestBody)
	// Getting user details for workFlowUpdate
	user := intercept.GetUserFromToken(c)
	if user == nil {
		// error message if user isn't fetched
		return helper.ErrorHandler(c)
	}
	if err := validatorInstance.Struct(&requestBody); err != nil {
		log.Errorf("invalid payload received - %v", err)
		return helper.SendGenericResponse(c, http.StatusBadRequest, utils.ConvertValidationErrToString(err).Error())
	}
	// Check whether category is present, followd by type check
	category, ok, _ := validatePCategoryIsPresent(requestBody.CategoryName)
	if !ok {
		return helper.SendGenericResponse(c, http.StatusNotFound,
			fmt.Sprintf(constants.RECORD_NOT_FOUND_ERROR+" for category - %v", requestBody.CategoryName))
	}
	// Validate distro ids'
	for _, distroID := range requestBody.DistributorIDs {
		if err := validateDistroID(distroID); err != nil {
			log.Errorf("Invalid distroID - %v received for category - %v", distroID, requestBody.CategoryName)
			return helper.SendGenericResponse(c, http.StatusNotFound,
				fmt.Sprintf(constants.RECORD_NOT_FOUND_ERROR+" for distro - %v", distroID))
		}
	}
	// Process update request
	switch requestBody.Type {
	case enums.UPDATE:
		for _, newDistroID := range requestBody.DistributorIDs {
			if !utils.Contains(category.DistributorIDs, newDistroID) {
				category.DistributorIDs = append(category.DistributorIDs, newDistroID)
			}
		}
	case enums.REPLACE:
		var newDistroList []string
		for _, newDistroID := range requestBody.DistributorIDs {
			newDistroList = append(newDistroList, newDistroID)
		}
		category.DistributorIDs = newDistroList
	}
	category.WorkFlowStatus = append(category.WorkFlowStatus, *models.NewWorkFlowStatus(user.Email, enums.UPDATED))
	if err := iProductCategoryDAO.Update(category); err != nil {
		log.Errorf("Record update error - %v for category - %v in collection - %v",
			err, requestBody.CategoryName, constants.PRODUCT_CATEGORY)
		return helper.ErrorHandler(c)
	}
	log.Info("Exiting ProductTypeHandler.UpdateCategory() function")
	return helper.SendGenericResponse(c, http.StatusOK, constants.RECORD_UPDATE_SUCCESS)
}

// FetchCategory will fetch product-category from database
// @Summary FetchCategory will fetch product-category from database
// @Description FetchCategory will fetch product-category from database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param CATEGORY header string false "fetch specific product-category details"
// @Success 200 {object} response.FetchProductCategoryResponse "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 404 {object} locales.ResponseTemplate "If record isn't found"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/category [get]
func (object *productcategoryHandler) FetchCategory(c echo.Context) error {
	log.Info("Entering ProductTypeHandler.UpdateCategory() function")
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	headerCategory := strings.TrimSpace(c.Request().Header.Get(constants.HEADER_CATEGORY))
	log.Info("Exiting ProductTypeHandler.UpdateCategory() function")
	return processGetProductCategoryDetails(c, headerCategory)
}

// DropCategory will delete product-category from database
// @Summary DropCategory will delete product-category from database
// @Description DropCategory will delete product-category from database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param DropProductCategoryRequest body request.DropProductCategoryRequest true "Updates existing productCategory"
// @Success 200 {object} response.FetchProductCategoryResponse "request processed sucessfully"
// @Success 202 {object} response.UpdateResponse "if request has some error"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/category [delete]
func (object *productcategoryHandler) DropCategory(c echo.Context) error {
	var (
		requestBody request.DropProductCategoryRequest
		errDetails  []response.Details
	)
	log.Info("Entering ProductTypeHandler.DropCategory() function")
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	if err := c.Bind(&requestBody); err != nil {
		log.Errorf("Request Body binding error - %v", err)
		return helper.ErrorHandler(c)
	}
	log.Infof("Request payload received - %+v", requestBody)
	user := intercept.GetUserFromToken(c)
	if user == nil {
		// error message if user isn't fetched
		return helper.ErrorHandler(c)
	}
	for _, eachCategory := range requestBody.Categories {
		productCategoryFromDB, ok, err := validatePCategoryIsPresent(eachCategory)
		if !ok {
			errDetails = append(errDetails, response.NewErrorDetail(http.StatusBadRequest, err.Error()))
			break
		}
		productCategoryFromDB.IsDeleted = true
		productCategoryFromDB.WorkFlowStatus = append(productCategoryFromDB.WorkFlowStatus,
			*models.NewWorkFlowStatus(user.Email, enums.DELETED))
		if err := iProductCategoryDAO.Update(productCategoryFromDB); err != nil {
			log.Errorf("Product category workflow update error - %v", err)
			return helper.ErrorHandler(c)
		}
	}
	log.Info("Exiting ProductTypeHandler.DropCategory() function")
	if len(errDetails) != 0 {
		return c.JSON(http.StatusAccepted,
			response.FormUpdateResponse(http.StatusBadRequest, constants.REQUEST_PROCESSING_ERROR, errDetails))
	}
	return helper.SendSuccessResponse(c)
}

// processGetProductCategoryDetails will process get product-category
func processGetProductCategoryDetails(c echo.Context, headerCategory string) error {
	var result response.FetchProductCategoryResponse
	log.Info("Entering ProductTypeHandler.processGetProductCategoryDetails() function")
	if utils.IsBlankString(headerCategory) {
		log.Info("Received Empty productCategory. Fetching all details ")
		// process fetch for all records
		categorySet, err := iProductCategoryDAO.FindAll()
		if err != nil {
			log.Infof("Entries fetch error - %v", err)
		}
		for _, eachCategory := range categorySet {
			unitDetails, err := formGetProductCategoryDetailsResponse(c, &eachCategory)
			if err != nil {
				return err
			}
			result.CategoryDetails = append(result.CategoryDetails, *unitDetails)
		}
	} else {
		categoryDetails, ok, _ := validatePCategoryIsPresent(headerCategory)
		if !ok {
			return helper.SendGenericResponse(c, http.StatusNotFound, constants.INVALID_PROUDCT_CATEGORY)
		}
		unitDetails, err := formGetProductCategoryDetailsResponse(c, categoryDetails)
		if err != nil {
			return err
		}
		result.CategoryDetails = append(result.CategoryDetails, *unitDetails)
	}
	result.Code = http.StatusOK
	result.Msg = constants.RECORD_FETCHED_SUCCESS
	log.Info("Exiting ProductTypeHandler.processGetProductCategoryDetails() function")
	return c.JSON(http.StatusOK, &result)
}

// formGetProductCategoryDetailsResponse will return response object from product-category
func formGetProductCategoryDetailsResponse(c echo.Context, categoryDetails *models.ProductCategory) (*response.PCategoryDetails, error) {
	log.Info("Entering ProductTypeHandler.formGetProductCategoryDetailsResponse() function")
	var specificCategoryDetails response.PCategoryDetails
	if err := utils.UseObjectMapper(categoryDetails, &specificCategoryDetails); err != nil {
		log.Errorf("Object Mapper Error - %v", err)
		return nil, helper.ErrorHandler(c)
	}
	for _, eachDistroID := range categoryDetails.DistributorIDs {
		var (
			distroResponse response.FetchDistro
		)
		distroDetails, err := iDistributorDAO.FindByID(eachDistroID)
		if err != nil {
			log.Errorf("Distro Fetch Error - %v", err)
			return nil, helper.ErrorHandler(c)
		}
		if err := utils.UseObjectMapper(distroDetails, &distroResponse); err != nil {
			log.Errorf("Object Mapper Error - %v", err)
			return nil, helper.ErrorHandler(c)
		}
		distroResponse.FormContactAppend()
		specificCategoryDetails.Distributors = append(specificCategoryDetails.Distributors, distroResponse)
	}
	log.Info("Exiting ProductTypeHandler.formGetProductCategoryDetailsResponse() function")
	return &specificCategoryDetails, nil
}

// validatePTypeIsPresent will check if given pType is present in the db or not
func validatePCategoryIsPresent(category string) (*models.ProductCategory, bool, error) {
	log.Info("Entering ProductTypeHandler.validatePCategoryIsPresent() function")
	if utils.IsBlankString(category) {
		log.Info("Empty category received")
		return nil, false, fmt.Errorf(constants.INVALID_PROUDCT_CATEGORY)
	}
	log.Infof("Recevied Product category - [%v]", category)
	productCategoryDetails, err := iProductCategoryDAO.FindByCategory(strings.TrimSpace(strings.ToUpper(category)))
	if err != nil {
		return nil, false, utils.CastDBError(err)
	}
	log.Info("Exiting ProductTypeHandler.validatePCategoryIsPresent() function")
	return productCategoryDetails, true, nil
}

// validateDistroID will validate distro id present in db
func validateDistroID(distroID string) error {
	log.Info("Entering ProductTypeHandler.validateDistroID() function")
	if _, err := hex.DecodeString(distroID); err != nil {
		return fmt.Errorf("invalid distro id received - %v", distroID)
	}
	if _, err := iDistributorDAO.FindByID(distroID); err != nil {
		return utils.CastDBError(err)
	}
	log.Info("Entering ProductTypeHandler.validateDistroID() function")
	return nil
}
