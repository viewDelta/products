package handlers

import (
	helper "bitbucket.org/viewDelta/service-core/app-helper"
	"bitbucket.org/viewDelta/service-core/constants"
	"bitbucket.org/viewDelta/service-core/constants/enums"
	"bitbucket.org/viewDelta/service-core/intercept"
	_ "bitbucket.org/viewDelta/service-core/locales"
	"bitbucket.org/viewDelta/service-core/locales/request"
	"bitbucket.org/viewDelta/service-core/locales/response"
	"bitbucket.org/viewDelta/service-core/models"
	product_mapping "bitbucket.org/viewDelta/service-core/repository/product-mapping"
	"bitbucket.org/viewDelta/service-core/utils"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
	"strings"
)

var (
	iProductMappingDAO = product_mapping.New()
)

type productMapping struct {
	context echo.Context
}

func NewProductMappingHandler() ProductMappingHandler {
	return &productMapping{}
}

// CreateMapping creates new product-mapping in database
// @Summary CreateMapping creates new product-mapping in database
// @Description  CreateMapping creates new product-mapping in database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param ProductMappingRequest body request.ProductMappingRequest true "Creates new productMapping"
// @Success 201 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing or Invalid or request body contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/type [post]
func (handler *productMapping) CreateMapping(c echo.Context) error {
	var reqBody request.ProductMappingRequest
	log.Info("Entering productMappingHandler.CreateMapping Function")
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	if err := c.Bind(&reqBody); err != nil {
		log.Errorf("Request Body binding error - %v", err)
		return helper.ErrorHandler(c)
	}
	log.Infof("Create ProductMapping request received - %v", reqBody.String())
	handler.context = c
	user := intercept.GetUserFromToken(c)
	if user == nil {
		log.Warn("No User Is Found assoicated with Token")
		// error message if user isn't fetched
		return helper.ErrorHandler(c)
	}
	err := handler.processProductMapping(&reqBody, user.Email, false)
	log.Info("Exiting productMappingHandler.CreateMapping Function")
	return err
}

// CreateMapping updates new product-mapping in database
// @Summary CreateMapping updates new product-mapping in database
// @Description  CreateMapping updates new product-mapping in database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param ProductMappingRequest body request.ProductMappingRequest true "Updates new productMapping"
// @Success 200 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing or Invalid or request body contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/type [put]
func (handler *productMapping) UpdateMapping(c echo.Context) error {
	var reqBody request.ProductMappingRequest
	log.Info("Entering productMappingHandler.UpdateMapping Function")
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	if err := c.Bind(&reqBody); err != nil {
		log.Errorf("Request Body binding error - %v", err)
		return helper.ErrorHandler(c)
	}
	log.Infof("Update ProductMapping request received - %v", reqBody.String())
	handler.context = c
	user := intercept.GetUserFromToken(c)
	if user == nil {
		log.Warn("No User Is Found assoicated with Token")
		// error message if user isn't fetched
		return helper.ErrorHandler(c)
	}
	err := handler.processProductMapping(&reqBody, user.Email, true)
	log.Info("Exiting productMappingHandler.UpdateMapping Function")
	return err
}

// FetchMapping fetches product-mapping from database
// @Summary FetchMapping fetches product-mapping from database
// @Description FetchMapping fetches product-mapping from database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param Product-Mapping header string false "Product-Mapping"
// @Success 200 {object} response.ProductMappingResponse "request processed sucessfully"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/type [get]
func (handler *productMapping) FetchMapping(c echo.Context) error {
	var res response.ProductMappingResponse
	log.Infof("Entering productMappingHandler.FetchMapping function")
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	productMapping := strings.TrimSpace(c.Request().Header.Get(constants.HEADER_PRODUCT_MAPPING))
	if !utils.IsBlankString(productMapping) && !utils.IsValidWithRegex(constants.REGEX_ALPHA, productMapping) {
		log.Errorf("Invalid Product-Mapping Received - %v", productMapping)
		return helper.SendGenericResponse(c, http.StatusBadRequest, constants.INVALID_PRODUCT_MAPPING)
	}
	if utils.IsBlankString(productMapping) {
		log.Infof("Received Request for Product-Mapping - %v", productMapping)
		allMappingFromDB, err := iProductMappingDAO.FindAll()
		if err != nil {
			return helper.ErrorLogger(c, err)
		}
		for _, eachMapping := range allMappingFromDB {
			if err := res.AppendData(&eachMapping); err != nil {
				log.Errorf("Mapper error - %v", err)
				return helper.ErrorHandler(c)
			}
		}
	} else {
		log.Info("Received Request for fetching all records")
		mappingFromDB, err := iProductMappingDAO.FindByMapping(productMapping)
		if err != nil {
			return helper.ErrorLogger(c, err)
		}
		if err := res.AppendData(mappingFromDB); err != nil {
			return helper.ErrorLogger(c, err)
		}
	}
	log.Infof("Exiting productMappingHandler.FetchMapping function")
	res.Code = http.StatusOK
	res.Msg = constants.RECORD_FETCHED_SUCCESS
	return c.JSON(http.StatusOK, &res)
}

// DropMapping deletes existing product-mapping in database
// @Summary DropMapping deletes existing product-mapping in database
// @Description  DropMapping deletes existing product-mapping in database
// @Tags products
// @Accept json
// @Produce json
// @Param TOKEN header string true "session token"
// @Param Product-Mapping header string true "Product-Mapping"
// @Success 200 {object} locales.ResponseTemplate "request processed sucessfully"
// @Failure 401 {object} locales.ResponseTemplate "Unauthroized Token"
// @Failure 400 {object} locales.ResponseTemplate "Either header is Missing or Invalid or request header contains some error"
// @Failure 404 {object} locales.ResponseTemplate "Either header is Missing or Invalid or request header contains some error"
// @Failure 500 {object} locales.ResponseTemplate "Internal Server Error"
// @Router /products/type [delete]
func (handler *productMapping) DropMapping(c echo.Context) error {
	log.Info("Entering productMappingHandler.DropMapping Function")
	if err := intercept.SecurityFilter(c); err != nil {
		return c.JSON(http.StatusUnauthorized,
			helper.CustomResponseObject(http.StatusUnauthorized, constants.UNAUTHORISED_ACCESS))
	}
	user := intercept.GetUserFromToken(c)
	if user == nil {
		log.Warn("No User Is Found assoicated with Token")
		// error message if user isn't fetched
		return helper.ErrorHandler(c)
	}
	productMapping := strings.TrimSpace(c.Request().Header.Get(constants.HEADER_PRODUCT_MAPPING))
	if utils.IsBlankString(productMapping) || !utils.IsValidWithRegex(constants.REGEX_ALPHA, productMapping) {
		log.Errorf("Empty Product-Mapping received")
		return helper.SendGenericResponse(c, http.StatusBadRequest, constants.INVALID_PRODUCT_MAPPING)
	}
	log.Infof("Request received for deletion of product-Mapping - {%v : %v}", constants.HEADER_PRODUCT_MAPPING, productMapping)
	mappingFromDB, err := iProductMappingDAO.FindByMapping(productMapping)
	if err != nil {
		log.Errorf("Record Fetch Error - %v", err)
		return helper.SendGenericResponse(c, http.StatusNotFound, constants.RECORD_NOT_FOUND_ERROR)
	}
	for _, eachProductCategory := range mappingFromDB.Categories {
		categoryFromDB, err := iProductCategoryDAO.FindByCategory(eachProductCategory)
		if err != nil {
			log.Errorf("Record Fetch Error - %v", err)
			return helper.ErrorHandler(c)
		}
		categoryFromDB.IsDeleted = true
		categoryFromDB.WorkFlowStatus = append(categoryFromDB.WorkFlowStatus,
			*models.NewWorkFlowStatus(user.Email, enums.DELETED))
		if err := iProductCategoryDAO.Update(categoryFromDB); err != nil {
			log.Errorf("Record Update Error - %v", err)
			return helper.ErrorHandler(c)
		}
	}
	mappingFromDB.IsDeleted = true
	mappingFromDB.WorkFlowStatus = append(mappingFromDB.WorkFlowStatus,
		*models.NewWorkFlowStatus(user.Email, enums.DELETED))
	if err := iProductMappingDAO.Update(mappingFromDB); err != nil {
		log.Errorf("Record deletion Error - %v", err)
		return helper.ErrorHandler(c)
	}
	log.Info("Exiting productMappingHandler.DropMapping Function")
	return helper.SendSuccessResponse(c)
}

// processProductMapping will update the process create/update request
func (handler *productMapping) processProductMapping(reqBody *request.ProductMappingRequest, email string, isUpdate bool) error {
	var productMapping models.ProductMapping
	log.Info("Entering productMappingHandler.processProductMapping Function")
	if err := validateProductMappingPayload(reqBody); err != nil {
		if err == fmt.Errorf(constants.APPLICATION_ERROR) {
			return helper.SendGenericResponse(handler.context, http.StatusInternalServerError, err.Error())
		}
		return helper.SendGenericResponse(handler.context, http.StatusBadRequest, err.Error())
	}
	if isUpdate {
		// Update request
		productMappingFromDB, err := iProductMappingDAO.FindByMapping(reqBody.Type)
		if err != nil {
			log.Errorf("DB Error While Fetching record - %v", err)
			return helper.SendGenericResponse(handler.context, http.StatusNotFound,
				fmt.Sprintf(constants.RECORD_NOT_FOUND_ERROR+" for productType - %v", reqBody.Type))
		}
		productMappingFromDB.Categories = reqBody.Categories
		productMappingFromDB.WorkFlowStatus = append(productMappingFromDB.WorkFlowStatus,
			*models.NewWorkFlowStatus(email, enums.UPDATED))
		if err := iProductMappingDAO.Update(productMappingFromDB); err != nil {
			log.Errorf("Record insertion Error - %v", err)
			return helper.ErrorHandler(handler.context)
		}
		log.Info("Exiting productMappingHandler.processProductMapping Function")
		return helper.SendGenericResponse(handler.context, http.StatusOK, constants.RECORD_PROCESSED_SUCCESS)
	} else {
		// Create request
		if _, err := iProductMappingDAO.FindByMapping(reqBody.Type); err != nil {
			log.Errorf("DB error received - %v", err)
			if err == mongo.ErrNoDocuments {
				// Create a new entry
				if err := utils.UseObjectMapper(reqBody, &productMapping); err != nil {
					log.Errorf("Object Mapper Error - %v", err)
					return helper.ErrorHandler(handler.context)
				}
				productMapping.WorkFlowStatus = append(productMapping.WorkFlowStatus, *models.NewWorkFlowStatus(email, enums.CREATED))
				if err := iProductMappingDAO.Insert(&productMapping); err != nil {
					log.Errorf("Record insertion Error - %v", err)
					return helper.ErrorHandler(handler.context)
				}
				log.Info("Exiting productMappingHandler.processProductMapping Function")
				return helper.SendGenericResponse(handler.context, http.StatusCreated, constants.RECORD_PROCESSED_SUCCESS)
			}
			return helper.ErrorHandler(handler.context)
		}
		return helper.SendGenericResponse(handler.context, http.StatusBadRequest,
			fmt.Sprintf(constants.DETAILS_ALREADY_PRESENT+" for product-mapping : %v", reqBody.Type))
	}
}

// validateProductMappingPayload will validate the payload for post/put request
func validateProductMappingPayload(reqBody *request.ProductMappingRequest) error {
	log.Info("Entering productMappingHandler.validateProductMappingPayload function")
	if err := formatRequestBody(reqBody); err != nil {
		return err
	}
	if err := validatorInstance.Struct(*reqBody); err != nil {
		log.Errorf("Validation Error - %v", err)
		return utils.ConvertValidationErrToString(err)
	}
	// Checking for empty slice or not
	categoryList, err := iProductCategoryDAO.FindAll()
	if err != nil {
		log.Errorf("Error while fetch record - %v", err)
		return fmt.Errorf(constants.APPLICATION_ERROR)
	}
	for _, eachProductCategory := range reqBody.Categories {
		isValidCategory := false
		for _, eachCategoryFromDB := range categoryList {
			if strings.EqualFold(eachCategoryFromDB.CategoryName, eachProductCategory) {
				isValidCategory = true
				break
			}
		}
		if !isValidCategory {
			return fmt.Errorf(constants.INVALID_PROUDCT_CATEGORY+" -%v", eachProductCategory)
		}
	}
	log.Info("Exiting productMappingHandler.validateProductMappingPayload function")
	return nil
}

// formatRequestBody will do the formatting for post/put payload for productMapping Request
func formatRequestBody(reqBody *request.ProductMappingRequest) error {
	log.Info("Entering productMappingHandler.formatRequestBody function")
	reqBody.Type = strings.TrimSpace(strings.ToUpper(reqBody.Type))
	for index, eachCategory := range reqBody.Categories {
		if utils.IsBlankString(eachCategory) {
			return fmt.Errorf(constants.INVALID_PAYLOAD)
		}
		reqBody.Categories[index] = strings.TrimSpace(strings.ToUpper(eachCategory))
	}
	log.Info("Exiting productMappingHandler.formatRequestBody function")
	return nil
}
