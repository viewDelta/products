FROM alpine:3.11
MAINTAINER Abinash Pattajoshi
ARG AppDir="/usr/local/service/"
ARG LogDir="/var/retailGo/logs/"
RUN mkdir -p ${AppDir}
RUN mkdir -p ${LogDir} && chmod -R 777 ${LogDir}
COPY bin/* ${AppDir}
RUN chmod +x ${AppDir}/products
EXPOSE 8088
VOLUME ${LogDir}
WORKDIR ${AppDir}
CMD ["./products"]