.PHONY: clean
clean:
	rm -rf bin

.PHONY: build
build: clean
	#Ubuntu
	#env GOOS=linux GOARCH=amd64 go build -o bin/products
	#Alpine:3.11
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/products
	cp *.yml bin/	
	chmod 0444 bin/*
	chmod +x bin/products
